import extendNuxtConfig from "@shopware-pwa/nuxt-module/config"

export default extendNuxtConfig({
  head: {
    title: "Cyfrowe.pl",
    meta: [{ hid: "description", name: "description", content: "" }],
  },
})
