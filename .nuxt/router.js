import Vue from 'vue'
import Router from 'vue-router'
import { normalizeURL, decode } from 'ufo'
import { interopDefault } from './utils'
import scrollBehavior from './router.scrollBehavior.js'

const _b0787052 = () => interopDefault(import('..\\.shopware-pwa\\source\\pages\\about.vue' /* webpackChunkName: "pages/about" */))
const _0cc2cc12 = () => interopDefault(import('..\\.shopware-pwa\\source\\pages\\account.vue' /* webpackChunkName: "pages/account" */))
const _2c3948fb = () => interopDefault(import('..\\.shopware-pwa\\source\\pages\\account\\addresses.vue' /* webpackChunkName: "pages/account/addresses" */))
const _fb41e9b2 = () => interopDefault(import('..\\.shopware-pwa\\source\\pages\\account\\addresses\\index.vue' /* webpackChunkName: "pages/account/addresses/index" */))
const _44e1950c = () => interopDefault(import('..\\.shopware-pwa\\source\\pages\\account\\addresses\\add\\index.vue' /* webpackChunkName: "pages/account/addresses/add/index" */))
const _4cad2b18 = () => interopDefault(import('..\\.shopware-pwa\\source\\pages\\account\\addresses\\add\\_id.vue' /* webpackChunkName: "pages/account/addresses/add/_id" */))
const _87a44248 = () => interopDefault(import('..\\.shopware-pwa\\source\\pages\\account\\orders.vue' /* webpackChunkName: "pages/account/orders" */))
const _33f14626 = () => interopDefault(import('..\\.shopware-pwa\\source\\pages\\account\\orders\\index.vue' /* webpackChunkName: "pages/account/orders/index" */))
const _8972cbe4 = () => interopDefault(import('..\\.shopware-pwa\\source\\pages\\account\\orders\\_id.vue' /* webpackChunkName: "pages/account/orders/_id" */))
const _4020f7c2 = () => interopDefault(import('..\\.shopware-pwa\\source\\pages\\account\\profile.vue' /* webpackChunkName: "pages/account/profile" */))
const _6aa9cc58 = () => interopDefault(import('..\\.shopware-pwa\\source\\pages\\account\\recover\\password.vue' /* webpackChunkName: "pages/account/recover/password" */))
const _44122efc = () => interopDefault(import('..\\.shopware-pwa\\source\\pages\\all-promotions.vue' /* webpackChunkName: "pages/all-promotions" */))
const _52f6276c = () => interopDefault(import('..\\.shopware-pwa\\source\\pages\\checkout.vue' /* webpackChunkName: "pages/checkout" */))
const _ddb9749a = () => interopDefault(import('..\\.shopware-pwa\\source\\pages\\login.vue' /* webpackChunkName: "pages/login" */))
const _428eee5c = () => interopDefault(import('..\\.shopware-pwa\\source\\pages\\newsletter-subscribe.vue' /* webpackChunkName: "pages/newsletter-subscribe" */))
const _7e477b4e = () => interopDefault(import('..\\.shopware-pwa\\source\\pages\\order-success.vue' /* webpackChunkName: "pages/order-success" */))
const _2f51e38d = () => interopDefault(import('..\\.shopware-pwa\\source\\pages\\payment-failure.vue' /* webpackChunkName: "pages/payment-failure" */))
const _d405db6e = () => interopDefault(import('..\\.shopware-pwa\\source\\pages\\register.vue' /* webpackChunkName: "pages/register" */))
const _7360c102 = () => interopDefault(import('..\\.shopware-pwa\\source\\pages\\reset-password.vue' /* webpackChunkName: "pages/reset-password" */))
const _b6c6ae26 = () => interopDefault(import('..\\.shopware-pwa\\source\\pages\\sale.vue' /* webpackChunkName: "pages/sale" */))
const _5890eb64 = () => interopDefault(import('..\\.shopware-pwa\\source\\pages\\search.vue' /* webpackChunkName: "pages/search" */))
const _2baf8f2a = () => interopDefault(import('..\\.shopware-pwa\\source\\pages\\wishlist.vue' /* webpackChunkName: "pages/wishlist" */))
const _309ba5f1 = () => interopDefault(import('..\\.shopware-pwa\\source\\pages\\registration\\confirm.vue' /* webpackChunkName: "pages/registration/confirm" */))
const _679bd4a9 = () => interopDefault(import('..\\.shopware-pwa\\source\\pages\\_.vue' /* webpackChunkName: "pages/_" */))

const emptyFn = () => {}

Vue.use(Router)

export const routerOptions = {
  mode: 'history',
  base: '/',
  linkActiveClass: 'nuxt-link-active',
  linkExactActiveClass: 'nuxt-link-exact-active',
  scrollBehavior,

  routes: [{
    path: "/about",
    component: _b0787052,
    meta: {"url":"http://localhost","origin":"http://localhost","host":"localhost","pathPrefix":"/","domainId":"65bffcef054d46748ba7a56d7261899f","currencyId":"b7d2554b0ce847cd82f3ac9bd1c0dfca","snippetSetId":"2973fc8b88cf4a31be908bd388761c6c","languageId":"809d99c69ba04a43bf29e548d3026c04","languageName":"Polski","languageLabel":"Polski","languageLocaleCode":"pl-PL"},
    name: "about_/about_domainId_65bffcef054d46748ba7a56d7261899f"
  }, {
    path: "/about",
    component: _b0787052,
    meta: {"url":"http://shopware-stage.firmaxl.pl:81","origin":"http://shopware-stage.firmaxl.pl:81","host":"shopware-stage.firmaxl.pl","pathPrefix":"/","domainId":"8065852de5894e03b2f1f8afd60f6f9a","currencyId":"b7d2554b0ce847cd82f3ac9bd1c0dfca","snippetSetId":"2973fc8b88cf4a31be908bd388761c6c","languageId":"809d99c69ba04a43bf29e548d3026c04","languageName":"Polski","languageLabel":"Polski","languageLocaleCode":"pl-PL"},
    name: "about_/about_domainId_8065852de5894e03b2f1f8afd60f6f9a"
  }, {
    path: "/account",
    component: _0cc2cc12,
    meta: {"url":"http://localhost","origin":"http://localhost","host":"localhost","pathPrefix":"/","domainId":"65bffcef054d46748ba7a56d7261899f","currencyId":"b7d2554b0ce847cd82f3ac9bd1c0dfca","snippetSetId":"2973fc8b88cf4a31be908bd388761c6c","languageId":"809d99c69ba04a43bf29e548d3026c04","languageName":"Polski","languageLabel":"Polski","languageLocaleCode":"pl-PL"},
    name: "account_/account_domainId_65bffcef054d46748ba7a56d7261899f",
    children: [{
      path: "addresses",
      component: _2c3948fb,
      children: [{
        path: "",
        component: _fb41e9b2,
        name: "addresses___65bffcef054d46748ba7a56d7261899f"
      }, {
        path: "add",
        component: _44e1950c,
        name: "addresses_add_65bffcef054d46748ba7a56d7261899f"
      }, {
        path: "add/:id",
        component: _4cad2b18,
        name: "addresses_add/:id_65bffcef054d46748ba7a56d7261899f"
      }]
    }, {
      path: "orders",
      component: _87a44248,
      children: [{
        path: "",
        component: _33f14626,
        name: "orders___65bffcef054d46748ba7a56d7261899f"
      }, {
        path: ":id",
        component: _8972cbe4,
        name: "orders_:id_65bffcef054d46748ba7a56d7261899f"
      }]
    }, {
      path: "profile",
      component: _4020f7c2,
      name: "undefined_profile_65bffcef054d46748ba7a56d7261899f"
    }, {
      path: "recover/password",
      component: _6aa9cc58,
      name: "undefined_recover/password_65bffcef054d46748ba7a56d7261899f"
    }]
  }, {
    path: "/account",
    component: _0cc2cc12,
    meta: {"url":"http://shopware-stage.firmaxl.pl:81","origin":"http://shopware-stage.firmaxl.pl:81","host":"shopware-stage.firmaxl.pl","pathPrefix":"/","domainId":"8065852de5894e03b2f1f8afd60f6f9a","currencyId":"b7d2554b0ce847cd82f3ac9bd1c0dfca","snippetSetId":"2973fc8b88cf4a31be908bd388761c6c","languageId":"809d99c69ba04a43bf29e548d3026c04","languageName":"Polski","languageLabel":"Polski","languageLocaleCode":"pl-PL"},
    name: "account_/account_domainId_8065852de5894e03b2f1f8afd60f6f9a",
    children: [{
      path: "addresses",
      component: _2c3948fb,
      children: [{
        path: "",
        component: _fb41e9b2,
        name: "addresses___8065852de5894e03b2f1f8afd60f6f9a"
      }, {
        path: "add",
        component: _44e1950c,
        name: "addresses_add_8065852de5894e03b2f1f8afd60f6f9a"
      }, {
        path: "add/:id",
        component: _4cad2b18,
        name: "addresses_add/:id_8065852de5894e03b2f1f8afd60f6f9a"
      }]
    }, {
      path: "orders",
      component: _87a44248,
      children: [{
        path: "",
        component: _33f14626,
        name: "orders___8065852de5894e03b2f1f8afd60f6f9a"
      }, {
        path: ":id",
        component: _8972cbe4,
        name: "orders_:id_8065852de5894e03b2f1f8afd60f6f9a"
      }]
    }, {
      path: "profile",
      component: _4020f7c2,
      name: "undefined_profile_8065852de5894e03b2f1f8afd60f6f9a"
    }, {
      path: "recover/password",
      component: _6aa9cc58,
      name: "undefined_recover/password_8065852de5894e03b2f1f8afd60f6f9a"
    }]
  }, {
    path: "/all-promotions",
    component: _44122efc,
    meta: {"url":"http://localhost","origin":"http://localhost","host":"localhost","pathPrefix":"/","domainId":"65bffcef054d46748ba7a56d7261899f","currencyId":"b7d2554b0ce847cd82f3ac9bd1c0dfca","snippetSetId":"2973fc8b88cf4a31be908bd388761c6c","languageId":"809d99c69ba04a43bf29e548d3026c04","languageName":"Polski","languageLabel":"Polski","languageLocaleCode":"pl-PL"},
    name: "all-promotions_/all-promotions_domainId_65bffcef054d46748ba7a56d7261899f"
  }, {
    path: "/all-promotions",
    component: _44122efc,
    meta: {"url":"http://shopware-stage.firmaxl.pl:81","origin":"http://shopware-stage.firmaxl.pl:81","host":"shopware-stage.firmaxl.pl","pathPrefix":"/","domainId":"8065852de5894e03b2f1f8afd60f6f9a","currencyId":"b7d2554b0ce847cd82f3ac9bd1c0dfca","snippetSetId":"2973fc8b88cf4a31be908bd388761c6c","languageId":"809d99c69ba04a43bf29e548d3026c04","languageName":"Polski","languageLabel":"Polski","languageLocaleCode":"pl-PL"},
    name: "all-promotions_/all-promotions_domainId_8065852de5894e03b2f1f8afd60f6f9a"
  }, {
    path: "/checkout",
    component: _52f6276c,
    meta: {"url":"http://localhost","origin":"http://localhost","host":"localhost","pathPrefix":"/","domainId":"65bffcef054d46748ba7a56d7261899f","currencyId":"b7d2554b0ce847cd82f3ac9bd1c0dfca","snippetSetId":"2973fc8b88cf4a31be908bd388761c6c","languageId":"809d99c69ba04a43bf29e548d3026c04","languageName":"Polski","languageLabel":"Polski","languageLocaleCode":"pl-PL"},
    name: "checkout_/checkout_domainId_65bffcef054d46748ba7a56d7261899f"
  }, {
    path: "/checkout",
    component: _52f6276c,
    meta: {"url":"http://shopware-stage.firmaxl.pl:81","origin":"http://shopware-stage.firmaxl.pl:81","host":"shopware-stage.firmaxl.pl","pathPrefix":"/","domainId":"8065852de5894e03b2f1f8afd60f6f9a","currencyId":"b7d2554b0ce847cd82f3ac9bd1c0dfca","snippetSetId":"2973fc8b88cf4a31be908bd388761c6c","languageId":"809d99c69ba04a43bf29e548d3026c04","languageName":"Polski","languageLabel":"Polski","languageLocaleCode":"pl-PL"},
    name: "checkout_/checkout_domainId_8065852de5894e03b2f1f8afd60f6f9a"
  }, {
    path: "/login",
    component: _ddb9749a,
    meta: {"url":"http://localhost","origin":"http://localhost","host":"localhost","pathPrefix":"/","domainId":"65bffcef054d46748ba7a56d7261899f","currencyId":"b7d2554b0ce847cd82f3ac9bd1c0dfca","snippetSetId":"2973fc8b88cf4a31be908bd388761c6c","languageId":"809d99c69ba04a43bf29e548d3026c04","languageName":"Polski","languageLabel":"Polski","languageLocaleCode":"pl-PL"},
    name: "login_/login_domainId_65bffcef054d46748ba7a56d7261899f"
  }, {
    path: "/login",
    component: _ddb9749a,
    meta: {"url":"http://shopware-stage.firmaxl.pl:81","origin":"http://shopware-stage.firmaxl.pl:81","host":"shopware-stage.firmaxl.pl","pathPrefix":"/","domainId":"8065852de5894e03b2f1f8afd60f6f9a","currencyId":"b7d2554b0ce847cd82f3ac9bd1c0dfca","snippetSetId":"2973fc8b88cf4a31be908bd388761c6c","languageId":"809d99c69ba04a43bf29e548d3026c04","languageName":"Polski","languageLabel":"Polski","languageLocaleCode":"pl-PL"},
    name: "login_/login_domainId_8065852de5894e03b2f1f8afd60f6f9a"
  }, {
    path: "/newsletter-subscribe",
    component: _428eee5c,
    meta: {"url":"http://localhost","origin":"http://localhost","host":"localhost","pathPrefix":"/","domainId":"65bffcef054d46748ba7a56d7261899f","currencyId":"b7d2554b0ce847cd82f3ac9bd1c0dfca","snippetSetId":"2973fc8b88cf4a31be908bd388761c6c","languageId":"809d99c69ba04a43bf29e548d3026c04","languageName":"Polski","languageLabel":"Polski","languageLocaleCode":"pl-PL"},
    name: "newsletter-subscribe_/newsletter-subscribe_domainId_65bffcef054d46748ba7a56d7261899f"
  }, {
    path: "/newsletter-subscribe",
    component: _428eee5c,
    meta: {"url":"http://shopware-stage.firmaxl.pl:81","origin":"http://shopware-stage.firmaxl.pl:81","host":"shopware-stage.firmaxl.pl","pathPrefix":"/","domainId":"8065852de5894e03b2f1f8afd60f6f9a","currencyId":"b7d2554b0ce847cd82f3ac9bd1c0dfca","snippetSetId":"2973fc8b88cf4a31be908bd388761c6c","languageId":"809d99c69ba04a43bf29e548d3026c04","languageName":"Polski","languageLabel":"Polski","languageLocaleCode":"pl-PL"},
    name: "newsletter-subscribe_/newsletter-subscribe_domainId_8065852de5894e03b2f1f8afd60f6f9a"
  }, {
    path: "/order-success",
    component: _7e477b4e,
    meta: {"url":"http://localhost","origin":"http://localhost","host":"localhost","pathPrefix":"/","domainId":"65bffcef054d46748ba7a56d7261899f","currencyId":"b7d2554b0ce847cd82f3ac9bd1c0dfca","snippetSetId":"2973fc8b88cf4a31be908bd388761c6c","languageId":"809d99c69ba04a43bf29e548d3026c04","languageName":"Polski","languageLabel":"Polski","languageLocaleCode":"pl-PL"},
    name: "order-success_/order-success_domainId_65bffcef054d46748ba7a56d7261899f"
  }, {
    path: "/order-success",
    component: _7e477b4e,
    meta: {"url":"http://shopware-stage.firmaxl.pl:81","origin":"http://shopware-stage.firmaxl.pl:81","host":"shopware-stage.firmaxl.pl","pathPrefix":"/","domainId":"8065852de5894e03b2f1f8afd60f6f9a","currencyId":"b7d2554b0ce847cd82f3ac9bd1c0dfca","snippetSetId":"2973fc8b88cf4a31be908bd388761c6c","languageId":"809d99c69ba04a43bf29e548d3026c04","languageName":"Polski","languageLabel":"Polski","languageLocaleCode":"pl-PL"},
    name: "order-success_/order-success_domainId_8065852de5894e03b2f1f8afd60f6f9a"
  }, {
    path: "/payment-failure",
    component: _2f51e38d,
    meta: {"url":"http://localhost","origin":"http://localhost","host":"localhost","pathPrefix":"/","domainId":"65bffcef054d46748ba7a56d7261899f","currencyId":"b7d2554b0ce847cd82f3ac9bd1c0dfca","snippetSetId":"2973fc8b88cf4a31be908bd388761c6c","languageId":"809d99c69ba04a43bf29e548d3026c04","languageName":"Polski","languageLabel":"Polski","languageLocaleCode":"pl-PL"},
    name: "payment-failure_/payment-failure_domainId_65bffcef054d46748ba7a56d7261899f"
  }, {
    path: "/payment-failure",
    component: _2f51e38d,
    meta: {"url":"http://shopware-stage.firmaxl.pl:81","origin":"http://shopware-stage.firmaxl.pl:81","host":"shopware-stage.firmaxl.pl","pathPrefix":"/","domainId":"8065852de5894e03b2f1f8afd60f6f9a","currencyId":"b7d2554b0ce847cd82f3ac9bd1c0dfca","snippetSetId":"2973fc8b88cf4a31be908bd388761c6c","languageId":"809d99c69ba04a43bf29e548d3026c04","languageName":"Polski","languageLabel":"Polski","languageLocaleCode":"pl-PL"},
    name: "payment-failure_/payment-failure_domainId_8065852de5894e03b2f1f8afd60f6f9a"
  }, {
    path: "/register",
    component: _d405db6e,
    meta: {"url":"http://localhost","origin":"http://localhost","host":"localhost","pathPrefix":"/","domainId":"65bffcef054d46748ba7a56d7261899f","currencyId":"b7d2554b0ce847cd82f3ac9bd1c0dfca","snippetSetId":"2973fc8b88cf4a31be908bd388761c6c","languageId":"809d99c69ba04a43bf29e548d3026c04","languageName":"Polski","languageLabel":"Polski","languageLocaleCode":"pl-PL"},
    name: "register_/register_domainId_65bffcef054d46748ba7a56d7261899f"
  }, {
    path: "/register",
    component: _d405db6e,
    meta: {"url":"http://shopware-stage.firmaxl.pl:81","origin":"http://shopware-stage.firmaxl.pl:81","host":"shopware-stage.firmaxl.pl","pathPrefix":"/","domainId":"8065852de5894e03b2f1f8afd60f6f9a","currencyId":"b7d2554b0ce847cd82f3ac9bd1c0dfca","snippetSetId":"2973fc8b88cf4a31be908bd388761c6c","languageId":"809d99c69ba04a43bf29e548d3026c04","languageName":"Polski","languageLabel":"Polski","languageLocaleCode":"pl-PL"},
    name: "register_/register_domainId_8065852de5894e03b2f1f8afd60f6f9a"
  }, {
    path: "/reset-password",
    component: _7360c102,
    meta: {"url":"http://localhost","origin":"http://localhost","host":"localhost","pathPrefix":"/","domainId":"65bffcef054d46748ba7a56d7261899f","currencyId":"b7d2554b0ce847cd82f3ac9bd1c0dfca","snippetSetId":"2973fc8b88cf4a31be908bd388761c6c","languageId":"809d99c69ba04a43bf29e548d3026c04","languageName":"Polski","languageLabel":"Polski","languageLocaleCode":"pl-PL"},
    name: "reset-password_/reset-password_domainId_65bffcef054d46748ba7a56d7261899f"
  }, {
    path: "/reset-password",
    component: _7360c102,
    meta: {"url":"http://shopware-stage.firmaxl.pl:81","origin":"http://shopware-stage.firmaxl.pl:81","host":"shopware-stage.firmaxl.pl","pathPrefix":"/","domainId":"8065852de5894e03b2f1f8afd60f6f9a","currencyId":"b7d2554b0ce847cd82f3ac9bd1c0dfca","snippetSetId":"2973fc8b88cf4a31be908bd388761c6c","languageId":"809d99c69ba04a43bf29e548d3026c04","languageName":"Polski","languageLabel":"Polski","languageLocaleCode":"pl-PL"},
    name: "reset-password_/reset-password_domainId_8065852de5894e03b2f1f8afd60f6f9a"
  }, {
    path: "/sale",
    component: _b6c6ae26,
    meta: {"url":"http://localhost","origin":"http://localhost","host":"localhost","pathPrefix":"/","domainId":"65bffcef054d46748ba7a56d7261899f","currencyId":"b7d2554b0ce847cd82f3ac9bd1c0dfca","snippetSetId":"2973fc8b88cf4a31be908bd388761c6c","languageId":"809d99c69ba04a43bf29e548d3026c04","languageName":"Polski","languageLabel":"Polski","languageLocaleCode":"pl-PL"},
    name: "sale_/sale_domainId_65bffcef054d46748ba7a56d7261899f"
  }, {
    path: "/sale",
    component: _b6c6ae26,
    meta: {"url":"http://shopware-stage.firmaxl.pl:81","origin":"http://shopware-stage.firmaxl.pl:81","host":"shopware-stage.firmaxl.pl","pathPrefix":"/","domainId":"8065852de5894e03b2f1f8afd60f6f9a","currencyId":"b7d2554b0ce847cd82f3ac9bd1c0dfca","snippetSetId":"2973fc8b88cf4a31be908bd388761c6c","languageId":"809d99c69ba04a43bf29e548d3026c04","languageName":"Polski","languageLabel":"Polski","languageLocaleCode":"pl-PL"},
    name: "sale_/sale_domainId_8065852de5894e03b2f1f8afd60f6f9a"
  }, {
    path: "/search",
    component: _5890eb64,
    meta: {"url":"http://localhost","origin":"http://localhost","host":"localhost","pathPrefix":"/","domainId":"65bffcef054d46748ba7a56d7261899f","currencyId":"b7d2554b0ce847cd82f3ac9bd1c0dfca","snippetSetId":"2973fc8b88cf4a31be908bd388761c6c","languageId":"809d99c69ba04a43bf29e548d3026c04","languageName":"Polski","languageLabel":"Polski","languageLocaleCode":"pl-PL"},
    name: "search_/search_domainId_65bffcef054d46748ba7a56d7261899f"
  }, {
    path: "/search",
    component: _5890eb64,
    meta: {"url":"http://shopware-stage.firmaxl.pl:81","origin":"http://shopware-stage.firmaxl.pl:81","host":"shopware-stage.firmaxl.pl","pathPrefix":"/","domainId":"8065852de5894e03b2f1f8afd60f6f9a","currencyId":"b7d2554b0ce847cd82f3ac9bd1c0dfca","snippetSetId":"2973fc8b88cf4a31be908bd388761c6c","languageId":"809d99c69ba04a43bf29e548d3026c04","languageName":"Polski","languageLabel":"Polski","languageLocaleCode":"pl-PL"},
    name: "search_/search_domainId_8065852de5894e03b2f1f8afd60f6f9a"
  }, {
    path: "/wishlist",
    component: _2baf8f2a,
    meta: {"url":"http://localhost","origin":"http://localhost","host":"localhost","pathPrefix":"/","domainId":"65bffcef054d46748ba7a56d7261899f","currencyId":"b7d2554b0ce847cd82f3ac9bd1c0dfca","snippetSetId":"2973fc8b88cf4a31be908bd388761c6c","languageId":"809d99c69ba04a43bf29e548d3026c04","languageName":"Polski","languageLabel":"Polski","languageLocaleCode":"pl-PL"},
    name: "wishlist_/wishlist_domainId_65bffcef054d46748ba7a56d7261899f"
  }, {
    path: "/wishlist",
    component: _2baf8f2a,
    meta: {"url":"http://shopware-stage.firmaxl.pl:81","origin":"http://shopware-stage.firmaxl.pl:81","host":"shopware-stage.firmaxl.pl","pathPrefix":"/","domainId":"8065852de5894e03b2f1f8afd60f6f9a","currencyId":"b7d2554b0ce847cd82f3ac9bd1c0dfca","snippetSetId":"2973fc8b88cf4a31be908bd388761c6c","languageId":"809d99c69ba04a43bf29e548d3026c04","languageName":"Polski","languageLabel":"Polski","languageLocaleCode":"pl-PL"},
    name: "wishlist_/wishlist_domainId_8065852de5894e03b2f1f8afd60f6f9a"
  }, {
    path: "/registration/confirm",
    component: _309ba5f1,
    meta: {"url":"http://localhost","origin":"http://localhost","host":"localhost","pathPrefix":"/","domainId":"65bffcef054d46748ba7a56d7261899f","currencyId":"b7d2554b0ce847cd82f3ac9bd1c0dfca","snippetSetId":"2973fc8b88cf4a31be908bd388761c6c","languageId":"809d99c69ba04a43bf29e548d3026c04","languageName":"Polski","languageLabel":"Polski","languageLocaleCode":"pl-PL"},
    name: "registration-confirm_/registration/confirm_domainId_65bffcef054d46748ba7a56d7261899f"
  }, {
    path: "/registration/confirm",
    component: _309ba5f1,
    meta: {"url":"http://shopware-stage.firmaxl.pl:81","origin":"http://shopware-stage.firmaxl.pl:81","host":"shopware-stage.firmaxl.pl","pathPrefix":"/","domainId":"8065852de5894e03b2f1f8afd60f6f9a","currencyId":"b7d2554b0ce847cd82f3ac9bd1c0dfca","snippetSetId":"2973fc8b88cf4a31be908bd388761c6c","languageId":"809d99c69ba04a43bf29e548d3026c04","languageName":"Polski","languageLabel":"Polski","languageLocaleCode":"pl-PL"},
    name: "registration-confirm_/registration/confirm_domainId_8065852de5894e03b2f1f8afd60f6f9a"
  }, {
    path: "/*",
    component: _679bd4a9,
    meta: {"url":"http://shopware-stage.firmaxl.pl:81","origin":"http://shopware-stage.firmaxl.pl:81","host":"shopware-stage.firmaxl.pl","pathPrefix":"/","domainId":"8065852de5894e03b2f1f8afd60f6f9a","currencyId":"b7d2554b0ce847cd82f3ac9bd1c0dfca","snippetSetId":"2973fc8b88cf4a31be908bd388761c6c","languageId":"809d99c69ba04a43bf29e548d3026c04","languageName":"Polski","languageLabel":"Polski","languageLocaleCode":"pl-PL"},
    name: "all_/*_domainId_8065852de5894e03b2f1f8afd60f6f9a"
  }, {
    path: "/*",
    component: _679bd4a9,
    meta: {"url":"http://localhost","origin":"http://localhost","host":"localhost","pathPrefix":"/","domainId":"65bffcef054d46748ba7a56d7261899f","currencyId":"b7d2554b0ce847cd82f3ac9bd1c0dfca","snippetSetId":"2973fc8b88cf4a31be908bd388761c6c","languageId":"809d99c69ba04a43bf29e548d3026c04","languageName":"Polski","languageLabel":"Polski","languageLocaleCode":"pl-PL"},
    name: "all_/*_domainId_65bffcef054d46748ba7a56d7261899f"
  }],

  fallback: false
}

export function createRouter (ssrContext, config) {
  const base = (config._app && config._app.basePath) || routerOptions.base
  const router = new Router({ ...routerOptions, base  })

  // TODO: remove in Nuxt 3
  const originalPush = router.push
  router.push = function push (location, onComplete = emptyFn, onAbort) {
    return originalPush.call(this, location, onComplete, onAbort)
  }

  const resolve = router.resolve.bind(router)
  router.resolve = (to, current, append) => {
    if (typeof to === 'string') {
      to = normalizeURL(to)
    }
    return resolve(to, current, append)
  }

  return router
}
